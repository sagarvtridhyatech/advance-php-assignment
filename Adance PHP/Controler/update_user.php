<?php

   session_start();
   $id = $_SESSION['id'];
   
   $update = new Update($id);
   class Update
   {
   	  function __construct($id)
  	{

      include '../Model/connection.php'; 
  		$sd = new Database();
  		$con=$sd -> connection();
        
  	  



     if(isset($_POST['btn']))
     {
    	$fname = isset($_POST['fname']) ? (string)$_POST['fname'] : "";
    	$lname = isset($_POST['lname']) ? (string)$_POST['lname'] : "";
    	$gender = isset($_POST['gender']) ? (string)$_POST['gender'] : "";
    	$phone_no = isset($_POST['phone_no']) ? (string)$_POST['phone_no'] : "";
    	$email = isset($_POST['email']) ? (string)$_POST['email'] : "";
    	$password = isset($_POST['password']) ? (string)$_POST['password'] : "";
    	$hobby = isset($_POST['hobby']) ? $_POST['hobby'] : "";

       	$hobby = implode(",",$hobby);

       	$imageUpload = ($_FILES['Upload_file']['name'])!=""  ? $_FILES['Upload_file']['name'] : "download.png"; 

       $target_dir = "../img/";
       $target_file = $target_dir . basename($_FILES["Upload_file"]["name"]);
       $uploadOk = 1;
       $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
       if(isset($_FILES['Upload_file']))
       {

          $check = getimagesize($_FILES["Upload_file"]["tmp_name"]);
          if($check !== false) {
              //echo "File is an image - " . $check["mime"] . ".";
              $uploadOk = 1;
          } else {
              echo "File is not an image.";
              $uploadOk = 0;

       }
       if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
       && $imageFileType != "gif" ) {
           echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
           $uploadOk = 0;
       }
       // Check if $uploadOk is set to 0 by an error
       if ($uploadOk == 0) {
           echo "Sorry, your file was not uploaded.";
       // if everything is ok, try to upload file
       } else {
           if (move_uploaded_file($_FILES["Upload_file"]["tmp_name"], $target_file)) {
               //echo "The file ". basename( $_FILES["imageUpload"]["name"]). " has been uploaded.";
           } else {
               echo "Sorry, there was an error uploading your file.";
           }

         }
   } 
        
    	$ad = $sd -> update_user($con, $id, $fname, $lname, $gender, $hobby, $phone_no, $email, $password, $imageUpload);
     
      
        if($ad)
        {
        	header("location: dashborad.php");
        }     
     }
   }
  }

?>