
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="../mystyle.css">
	<script src="../jquery-3.4.1.min.js"></script>
	<title>Form</title>
	
	
</head>
<body>
	<div class="form">
	<form method="post" action="update_user.php" enctype="multipart/form-data">

        <h1 class="rf">Form</h1>
        
		<input type="text" name="fname" value="<?php echo $dr['First_name'];?>" placeholder="First Name" id="fname">
        <p id="error_ftext">*Required fname</p>
		<input type="text" name="lname"  value="<?php echo $dr['Last_name'];?>" placeholder="Last Name" id="lname">
            <p id="error_ltext">*Required lname</p>

		<br>
		<br><center>Gender:
		<label> Male<input type="radio" name="gender" value="male" <?php  echo $dr['Gender'] == 'male' ? "checked" : ''; ?> id="male"></label>
		<label> Female<input type="radio" name="gender" value="female" id="male" <?php  echo $dr['Gender'] == 'female' ? "checked" : ''; ?>>      </label>
        

        <br>
		<br>

		<?php  $hobby=explode(",", $dr['Hobby']); ?>
        Hobby:
        <label>Reading<input type="checkbox" name="hobby[]" value="Reading" <?php  echo (in_array("Reading", $hobby)  ? 'checked' : ''); ?> >     </label>
        <label>playing<input type="checkbox" name="hobby[]" value="playing" <?php  echo (in_array("playing", $hobby) ? 'checked' : ''); ?> >     </label>
        <label>NetSurfing<input type="checkbox" name="hobby[]" value="NetSurfing" <?php  echo (in_array("NetSurfing", $hobby)  ? 'checked' : ''); ?>></label>
        
		</center>

		
		<input type="email" name="email" value="<?php echo $dr['Email'];?>" placeholder="Email" id="email">
		<p id="error_email">*Email is not valid</p>
		<input type="number" name="phone_no" value="<?php echo $dr['Mobile_number'];?>" maxlength="10"  placeholder="Phone-no"id="phone">
		<p id="error_num">*Phone Number is not valid</p>
		<input type="password" name="password"  placeholder="password" id="pass1">
		<p id="error_pass1">*Password is not valid</p>
		<input type="password" name="confirm-pass" placeholder="confirm-pass" id="pass2">
		<p id="error_pass2">*Password is not match</p><br><br>
         <center>Profile picture:</center>
        <input type="file" name="Upload file" id="file">
		<input type="submit" class="btn" name="btn">
	
	</form>
	</div>

</body>

<script>
    	$(document).ready(function(){
    		$("p").hide();

    		$("#phone").blur(function(){ 
    			var number = $(this).val();
    			if(number=='')
    			{
    				 $("#error_num").show();
    			}
    			else if(number.length != 10)
    			{
    				$("#error_num").show();
    			}
    			else
    			{
    				$("#error_num").hide();
    			}
    		});
            
            
            $("#fname").blur(function(){
                var fname = $(this).val();
                if(fname=='')
                {
                     $("#error_ftext").show();
                }
                else
                {
                     $("#error_ftext").hide();
                }
                });
            $("#lname").blur(function(){
                var lname = $(this).val();
                if(lname=='')
                {
                     $("#error_ltext").show();
                }
                else
                {
                     $("#error_ltext").hide();
                } 
                });       

    		$("#email").blur(function(){
    			var email = $(this).val(); 
    			var email_len = email.split('.').reverse();

    			
    			if(email=='')
    			{
    				$("#error_email").show(); 
    			}
    			else if(email_len[0].length > 3)
    			{
    				$("#error_email").show();
    			}
    			else
    			{
    				$("#error_email").hide();

    			}

    		});

    		$("#pass1").blur(function(){
    			var pass1 = $(this).val();
    			

    			if(pass1 != '')
    			{
    				if(pass1.length < 8)
    				{
    					$("#error_pass1").show();
    				}
    				else
    				{
    					$("#error_pass1").hide();
    				}

    				re = /[0-9]/;
	                if(!re.test(pass1))
	                 {
    					$("#error_pass1").show();
	                 }
	                 else
    				{
    					$("#error_pass1").hide();
    				}

	                 re = /[a-z]/;
	                if(!re.test(pass1))
	                 {
    					$("#error_pass1").show();
	                 }
	                 else
    				{
    					$("#error_pass1").hide();
    				}

	                 re = /[A-Z]/;
	                if(!re.test(pass1))
	                 {
    					$("#error_pass1").show();
	                 }
	                 else
    				{
    					$("#error_pass1").hide();
    				}

	                 re= /[@#$!%^&*]/;
	                if(!re.test(pass1))
	                 {
    					$("#error_pass1").show();
	                 }
	                 else
    				{
    					$("#error_pass1").hide();
    				}	

    			}
    			else
    			{
    				$("#error_pass1").show();

    			}

    		});

    		$("#pass2").blur(function(){
    			var pass2 = $(this).val();
    			var pass1 =$("#pass1").val();

                if(pass1 != pass2)
                {
                	$("#error_pass2").show();
                } 
                else
                {
                	$("#error_pass2").hide();

                }  
           });
           

           $(".btn").click(function(){
           
           var gender=$("#male:checked").val();
           /*var hobby = [];
           $.each($("input[name='hobby']:checked"),function(){
           	hobby.push($(this).val());
          
            });*/

            var phone =$("#phone").val();
            var email =$("#email").val();
            var fname =$("#fname").val();
            var lname =$("#lname").val();
            
            var pass1 =$("#pass1").val();

          
           });

           
    	
    	});
		 

	</script>

</html>
